package ru.t1.dkandakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkandakov.tm.dto.request.project.ProjectUpdateByIndexRequest;
import ru.t1.dkandakov.tm.util.TerminalUtil;

public final class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final ProjectUpdateByIndexRequest request = new ProjectUpdateByIndexRequest(getToken(), index, name, description);
        getProjectEndpoint().updateProjectByIndex(request);
    }

    @NotNull
    @Override
    public String getName() {
        return "project-update-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update project by index.";
    }

}