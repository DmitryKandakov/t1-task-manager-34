package ru.t1.dkandakov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    @Nullable
    Project create(@Nullable String userId, @NotNull String name, @NotNull String description);

    @NotNull
    Project create(@NotNull String userId, @NotNull String name);

}