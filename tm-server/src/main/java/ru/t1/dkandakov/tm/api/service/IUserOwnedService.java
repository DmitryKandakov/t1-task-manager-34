package ru.t1.dkandakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IService<M> {

    @Nullable
    M add(String userId, M model);

    @NotNull
    List<M> findAll(String userId);

    @NotNull
    List<M> findAll(String userId, Comparator<M> comparator);

    @NotNull
    M findOneById(String userId, String id);

    @NotNull
    M findOneByIndex(String userId, Integer index);

    @Nullable
    M removeOne(String userId, M model);

    @Nullable
    M removeOneById(String userId, String id);

    @Nullable
    M removeOneByIndex(String userId, Integer index);

    void removeAll(String userId);

    int getSize(@Nullable String userId);

    boolean existsById(@Nullable String userId, @Nullable String id);

}